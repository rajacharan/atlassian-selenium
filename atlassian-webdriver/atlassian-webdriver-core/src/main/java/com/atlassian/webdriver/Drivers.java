package com.atlassian.webdriver;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.openqa.selenium.WebDriver;

import java.util.function.Supplier;

/**
 * Utils for getting and manipulating {@link org.openqa.selenium.WebDriver} instances.
 *
 * @since 2.1
 */
public final class Drivers
{

    private Drivers()
    {
        throw new AssertionError("Don't instantiate me");
    }


    public static Supplier<WebDriver> fromProduct(final TestedProduct<? extends WebDriverTester> product)
    {
        return () -> product.getTester().getDriver();
    }
}

package com.atlassian.webdriver.debug;

import com.atlassian.webdriver.utils.WebDriverUtil;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.UnsupportedCommandException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.StreamSupport;

/**
 * Default implementation of {@link JavaScriptErrorRetriever}.
 *
 * @since 2.3
 */
public class DefaultJavaScriptErrorRetriever implements JavaScriptErrorRetriever {
    private static final Map<SessionId, Boolean> errorRetrievalSupportedCache = new ConcurrentHashMap<>();

    private final Supplier<? extends WebDriver> webDriver;

    public DefaultJavaScriptErrorRetriever(Supplier<? extends WebDriver> webDriver) {
        this.webDriver = webDriver;
    }

    @Override
    public boolean isErrorRetrievalSupported() {
        SessionId sessionId = WebDriverUtil.as(webDriver.get(), RemoteWebDriver.class).getSessionId();
        return errorRetrievalSupportedCache.computeIfAbsent(sessionId, ignored -> {
            try {
                return webDriver.get().manage().logs().getAvailableLogTypes().contains(LogType.BROWSER);
            } catch (UnsupportedCommandException e) {
                return false;
            }
        });
    }

    @Override
    public Iterable<JavaScriptErrorInfo> getErrors() {
        if (isErrorRetrievalSupported()) {
            return () -> StreamSupport.stream(webDriver.get().manage().logs().get(LogType.BROWSER).spliterator(), false)
                    .map(entry -> (JavaScriptErrorInfo) new JavaScriptErrorInfo() {
                                public String getDescription() {
                                    return entry.toString();
                                }

                                public String getMessage() {
                                    return entry.getMessage();
                                }
                            }
                    ).iterator();
        } else {
            return ImmutableList.of();
        }
    }
}

package com.atlassian.webdriver.it.pageobjects.page;

import com.atlassian.pageobjects.Page;

public class ElementDisplayednessPage implements Page {
    public String getUrl() {
        return "/html/element-displayedness.html";
    }
}
